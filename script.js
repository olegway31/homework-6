class GameField {
  // Информация о клетках поля
  state = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ];

  winnerCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  // Крестик или Нолик. Хранит активного игрока. Первые крестики ходят
  mode = 'x';
  // Состояние. Игра закончена или нет;
  isOverGame = false;
  // Состояние. Кто победил / Ничья;
  statusStateGame = 'Неясно';

  getGameFieldStatus() {
    // Победили Крестики Или победили Нолики
    console.log(this.statusStateGame, this.state);
  }
  // Метод, для изменения активного игрока
  setMode(currentMode) {
    currentMode === 'x' ? (this.mode = 'o') : (this.mode = 'x');
  }
  // Есть ли вообще такая строка или столбец
  _hasAvailableRange(value) {
    return value >= 0 && value <= 2 ? true : false;
  }
  // Занята ли та ячейка в которую мы хотим поставить символ ?
  _hasAvailableState(row, column) {
    return this.state[row][column] === null ? true : false;
  }

  setFieldCellValue() {
    let row = prompt('Введите номер Строки', 0);
    if (!this._hasAvailableRange(row)) {
      alert('Нет такой строки!');
      return false;
    }

    let column = prompt('Введите номер Столбца', 0);
    if (!this._hasAvailableRange(column)) {
      alert('Нет такого столбца!');
      return false;
    }

    if (!this._hasAvailableState(row, column)) {
      alert('Занято!!! Давай поновой');
      return false;
    }

    return (this.state[row][column] = this.mode);
  }

  // Проверка есть ли Нулевое значение. Если есть то игра незакончена
  _hasNullValue(squares) {
    for (let cell of squares) {
      if (cell === null) return true;
    }
    return false;
  }

  // Вычислить победителя в игре
  calculateWinners() {
    const currentGameField = this.state;
    const lines = this.winnerCombinations;
    const squares = [...currentGameField.flat()];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        this.statusStateGame = 'Победили -> ' + squares[a];
        return this.statusStateGame;
      }
    }

    if (!this._hasNullValue(squares)) {
      this.statusStateGame = 'Ничья';
      return this.statusStateGame;
    }

    return null;
  }
}

// ==================================
// ============== Main ==============
// ==================================
let gameField = new GameField();
console.log(gameField.mode);

while (!gameField.isOverGame) {
  if (!gameField.setFieldCellValue()) continue;

  gameField.setMode(gameField.mode);
  if (gameField.calculateWinners()) {
    gameField.isOverGame = true;
    break;
  }
}
// Вывод сообщения кто победил.
gameField.getGameFieldStatus();
